# Input&trade; solutions

Precomputed solutions from input-solver. Useful for playing around
with play-input without needing to run solve-input on your computer.

These data files are probably not very useful without the code in the
input-solver repository.

## License

input-solutions is licensed under the GNU Affero General Public
License Version 3. See the `LICENSE` file for more information.

    input-solutions, a set of game-theoretic solutions for the board game of Input(tm)
    Copyright (C) 2021  Frank J. T. Wojcik

    This program is free software: you can redistribute it and/or
    modify it under the terms of the GNU Affero General Public License
    version 3 as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Disclaimer

Input&trade; is a trademark of Milton Bradley. Use of the trademark
does not imply any affiliation with or endorsement by them.
